import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Products from '../pages/Products';


export default function ProductCard({productProp}){

	const {name,description,price,quantity,_id}= productProp

	return(

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Card.Subtitle>Quantity</Card.Subtitle>
				<Card.Text>{quantity}</Card.Text>

				<Link className ="btn btn-success" to={`/${_id}`}>Details</Link>

			</Card.Body>
		</Card>

		)
}