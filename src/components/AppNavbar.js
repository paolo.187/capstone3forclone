import {Fragment, useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
// import Navbar from 'react-bootstrap/Navbar'
// import Nav from 'react-bootstrap/Nav'
import '../App.css';



export default function AppNavbar(){
		return(
			<Navbar bg="light" expand="lg">
				<Navbar.Brand as={Link} to="#">blackPlease... </Navbar.Brand>
				<Navbar.Toggle aria-controls ="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav"> 
					<Nav className="mr-auto">
								<Nav.Link as={Link} to='/'>Home</Nav.Link>
								<Nav.Link as={Link} to='/products'>Products</Nav.Link>
								<Nav.Link as={Link} to='/login'>Login</Nav.Link>

								<Nav.Link as={Link} to="/register" exact>Register</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>





			)
}