
import {Fragment,useEffect,useState} from 'react';

import ProductCard from '../components/ProductCard';


export default function Products(){

	const [product,setProduct] = useState([])

	useEffect(()=>{
		fetch('http://localhost:4000/products/allProducts')
		.then(res=>res.json())
		.then(data=>{

			setProduct(data.map(product=>{

				return(
						<ProductCard key = {product.id} productProp= {product} />
					)


			}))
		})

	},[])



	return(
		<Fragment>
			<h1>Products</h1>
			{product}
		</Fragment>

		)
}