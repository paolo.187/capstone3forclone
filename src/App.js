
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom'
import {useState,useEffect} from 'react'
import {Container} from 'react-bootstrap'

import{Route, Switch} from 'react-router-dom'

import AppNavbar from './components/AppNavbar'
import ProductCard from './components/ProductCard'
import NotFound from './components/NotFound'

import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import {UserProvider} from './UserContext'


function App() {


const [user,setUser]= useState({

    id:null,
    isAdmin:null
})

const unsetUser = () =>{
    localStorage.clear()
}

useEffect(()=>{

    let token = localStorage.getItem('token');
    fetch('http://localhost:4000/users/details',{
        method:'GET',
        headers:{
            Authorization:`Bearer ${token}`
        }
    })
    .then(res=>res.json())
    .then(data=>{
        console.log(data)

        if (typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id:null,
          isAdmin: null
        })
      }
    })
},[])


 return(
        <UserProvider value={{user,setUser,unsetUser}}>
            

        
 		<Router>
 			<AppNavbar/>
 			

            <Container>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/products' component={Products} />
                    <Route exact path="/login" component={Login}/>
                   <Route exact path="/register" component={Register}/>
                  
                    <Route component={NotFound} />
                    
                </Switch>
                
            </Container>
 		</Router>
        </UserProvider>

 	)
}

export default App;
