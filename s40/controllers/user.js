const	User = require("../models/User");
const	bcrypt = require("bcrypt");
const	auth = require ("../auth");
const	Product = require("../models/Product");


//register user


module.exports.registerUser = (reqBody)=> {

	return User.find({email: reqBody.email}).then(result =>{
		if(result.length >0){
			return false
			// return "already registered"
		}else {
			
			let newUser = new User ({
		
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password,10)
		
			})

			return newUser.save().then((user,error)=>{
			if (error) {
				return false
		}else{
			return true
		}
			})

		}
	})
}


//get USER details
module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};



//get ALL users

module.exports.allUsers = ()=>{
	return User.find().then(result=> {
		return result
	})
}



//user LOGIN (authentication)
module.exports.loginUser = (reqBody)=> {

	return User.findOne({email:reqBody.email}).then(result => {

		if(result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}

			}
			else{
				return false
			}
		}
	})
}

//Admin sets user as Admin

module.exports.setAsAdmin = (reqParams, reqBody) => {
	let userSetForAdmin = {
		isAdmin:true,
		
	}
	//findByIdAndUpdate (document,updatesToBeAplied)
	return User.findByIdAndUpdate(reqParams.userId, userSetForAdmin).then((user,err)=>{
		if (err){
			return false
		}else {
			return true
		}
	})
}


//non Admin user making order






module.exports.order = async(data) => {

	let nameOfProduct = await Product.findById(data.productId).then(result=>{
					return result.name
				});	

	let productPrice = await Product.findById(data.productId).then(result=>{
					return result.price
				});	


	let isUserUpdated = await User.findById(data.userId).then(user=>{

				
			user.orders.push({productId: data.productId,productName: nameOfProduct ,quantity:data.quantity, total:(productPrice*data.quantity)});

			return user.save().then((user,error)=> {
				if (error) {
					return false
				}else {
					return true
				}
			})
	})
	
	let isProductUpdated = await Product.findById(data.productId).then(product => {
			
			product.quantity -= data.quantity

			product.orders.push({userId: data.userId, orderQuantity:data.quantity ,orderTotal:productPrice*data.quantity });
		
			return product.save().then((product,error) =>{
				if (error){
					return false
				}else {
					return true
				}
			})
	})

	if(isUserUpdated && isProductUpdated) {
		return true
	}

	else {
		return false
	}

}


//retrieve Authenticated users orders (checkout)
		
module.exports.getUserOrders = async (userData, reqParams) => {

	

	return User.findById(reqParams.userId).then(result => {

		if (result.email == userData.email) {
			return result.orders
		}else
		 return "Your authentication does not match customer ID"
		//show only ID, email, and orders
		// let userDetails = {
		// 	email: result.email,
		// 	id: result._id,
		// 	orders:result
		// }
		// return userDetails
		
	})
}

//get ALL Orders (Admin)
module.exports.allOrders = ()=>{
	return User.find({isAdmin:false},{email:0,password:0,isAdmin:0 }).then(result=> {
		return result
	})
}
