const	express = require("express");
const	router = express.Router();
const	userController = require("../controllers/user")
const	auth = require ("../auth");


//user registration
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})




//get User details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



//get ALL users



router.get("/allUsers", auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization)
	userController.allUsers().then(resultFromController=>
		res.send(resultFromController))
})		



// get ALL Orders (Admin)
router.get("/allOrders", auth.verify,(req,res)=> {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == true){

		userController.allOrders().then(resultFromController=>
		res.send(resultFromController))

	}else{

		console.log("not authorized")
			res.send("not authorized")
	}
	
})		




//user LOGIN (authentication)
router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController));
})


//Admin sets user as Admin

router.put("/:userId/setAsAdmin", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization)


		if (userData.isAdmin == true){
			userController.setAsAdmin(req.params, req.body).then(resultFromController =>
			res.send(resultFromController))
		}else{
			console.log("not authorized")
			res.send("not authorized")
		}

	
})

// CREATE ORDER (non Admin user checkout)

router.post("/order",auth.verify,(req,res) => {
		const userData = auth.decode(req.headers.authorization)

		if (userData.isAdmin == true){
			res.send("Admin not authorized to order")
		}else {

			let data = {
				//so logged in only user can enroll himself
				userId:userData.id,
				productId:req.body.productId,
				quantity:req.body.quantity

			}

			userController.order(data).then(resultFromController =>
				res.send(resultFromController))


		}

});

//retrieve Authenticated users orders(checkout)


// router.get("/:userId",auth.verify, (req,res)=> {
	
// 	console.log(req.params.userId)

// 	userController.getUserOrders(req.params).then(resultFromController =>
// 		res.send(resultFromController))
// });


router.get("/:userId",auth.verify, (req,res)=> {
	
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == true){
			res.send("Admin not authorized to checkout")
		}else{

			userController.getUserOrders(userData,req.params).then(resultFromController =>
				res.send(resultFromController))

		}

});








module.exports = router;